<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\Entity;

use Allmega\BlogBundle\Entity\Tag;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\ItemInfo;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Model\SortableItemInterface;
use Allmega\AnnouncementBundle\Repository\AnnouncementRepository;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: AnnouncementRepository::class)]
#[ORM\Table(name: '`allmega_announcement`')]
class Announcement extends ItemInfo implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $publish = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $deletion = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 3000, maxMessage: 'errors.max_value')]
    private ?string $message = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;

    #[Assert\Count(max: 4, maxMessage: 'errors.too_many_tags')]
    #[ORM\ManyToMany(targetEntity: Tag::class, cascade: ['persist'])]
    #[ORM\JoinTable(name: '`allmega_announcement__tag`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $tags;

    /**
     * Create a new Announcement entity with predetermined data,
     * if no data is provided, it will be generated:
     * - $message, $title as dummy text
     * - $user will be created
     */
    public static function build(
        string $message = null,
        bool $active = false,
        User $user = null,
        string $title = null): static
    {
        $title = $title ?? Helper::generateRandomString();
        $message = $message ?? Helper::getLoremIpsum();
        $user = $user ?? User::build();

        return (new static())
            ->setMessage($message)
            ->setActive($active)
            ->setCreator($user)
            ->setEditor($user)
            ->setTitle($title);
    }

    public function __construct()
    {
        parent::__construct();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPublish(): ?DateTimeInterface
    {
        return $this->publish;
    }

    public function setPublish(?DateTimeInterface $publish): static
    {
        $this->publish = $publish;
        return $this;
    }

    public function getDeletion(): ?DateTimeInterface
    {
        return $this->deletion;
    }

    public function setDeletion(?DateTimeInterface $deletion): static
    {
        $this->deletion = $deletion;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return Collection<int,Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag ...$tags): void
    {
        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) $this->tags->add($tag);
        }
    }

    public function removeTag(Tag $tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function setMediaDir(): void
    {
        $this->mediaDir = 'announcements_'.$this->id;
    }

    public static function getSortableProps(): array
    {
        return ['title', 'publish', 'deletion', 'active', 'created', 'updated'];
    }

    public static function getBundleName(): string
    {
        return 'Announcement';
    }
}