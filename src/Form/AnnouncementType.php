<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\Form;

use Allmega\AnnouncementBundle\Data;
use Allmega\MediaBundle\Form\FileLoadType;
use Allmega\AnnouncementBundle\Entity\Announcement;
use Allmega\BlogBundle\Form\Type\{DateTimePickerType, TagsInputType};
use Symfony\Component\Form\{FormBuilderInterface, AbstractType};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnouncementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('publish', DateTimePickerType::class, [
                'label' => 'announcement.label.publish',
                'help' => 'announcement.help.publish',
                'required' => false
            ])
            ->add('deletion', DateTimePickerType::class, [
                'label' => 'announcement.label.deletion',
                'help' => 'announcement.help.deletion',
                'required' => false
            ])
            ->add('title', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'announcement.label.title' ],
                'label' => 'announcement.label.title'
            ])
            ->add('message', TextareaType::class, [
                'attr' => ['rows' => 5, 'placeholder' => 'announcement.label.message'],
                'label' => 'announcement.label.message'
            ])
            ->add('active', null, [
                'label' => 'announcement.label.active',
                'help' => 'announcement.help.active'
            ])
            ->add('tags', TagsInputType::class, [
                'label' => 'announcement.label.tags',
                'help'  => 'announcement.help.tags',
                'required' => false
            ])
            ->add('files', FileLoadType::class, [
                'attr' => ['multiple' => true],
                'required' => false,
                'mapped' => false,
                'label' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Announcement::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}