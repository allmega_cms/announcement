<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\AnnouncementBundle\Entity\Announcement;

class AnnouncementsLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.announcements.message';
        $this->title = 'loader.announcements.phrase';
        $this->class = Announcement::class;
        $this->prop = 'title';
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($existsItem) {
            $item = $existsItem
                ->setMessage($item->getMessage())
                ->setCreator($item->getCreator())
                ->setEditor($item->getEditor())
                ->setActive($item->getActive());
        }
    }
}