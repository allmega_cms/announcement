<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\Utils\Loader;

use Allmega\AnnouncementBundle\Utils\Loader\Loaders\AnnouncementsLoader;
use Allmega\BlogBundle\Utils\Loader\Model\AbstractStorage;

class Storage extends AbstractStorage
{
    protected array $props = ['announcements'];
    protected array $announcements = [];

    public function getLoaders(): array
    {
        return [
            AnnouncementsLoader::class => $this->announcements,
        ];
    }
}