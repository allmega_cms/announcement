<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle;

use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntriesMap, ControllerEntry};
use Allmega\AnnouncementBundle\Controller\AnnouncementController;
use Allmega\BlogBundle\Utils\Loader\Entries\MenupointEntry;
use Allmega\BlogBundle\Model\PackageData;
use Allmega\BlogBundle\Data as BlogData;

class Data extends PackageData
{
	public const DOMAIN = 'AllmegaAnnouncementBundle';
	public const PACKAGE = 'announcement';

    public const NOTIFICATION_ANNOUNCEMENT_ENABLED = 'announcement_enabled';

	public const GROUP_TYPE_ANNOUNCEMENT = 'announcement.main';

    public const MANAGER_GROUP = 'announcement.manager';
	public const AUTHOR_GROUP = 'announcement.author';
    public const USER_GROUP = 'announcement.user';

	public const MANAGER_ROLE = 'ROLE_ANNOUNCEMENT_MANAGER';
	public const AUTHOR_ROLE = 'ROLE_ANNOUNCEMENT_AUTHOR';
	public const USER_ROLE = 'ROLE_ANNOUNCEMENT_USER';

    protected function setRegisterData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'controllerEntries' => $this->getControllerEntries(),
        ];
    }

    protected function setLoadData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'notificationtypes' => $this->getNotificationTypes(),
            'menupoints' => $this->getMenuPoints(),
            'grouptypes' => $this->getGroupTypes(),
            'groups' => $this->getGroups(),
            'roles' => $this->getRoles(),
            'users' => $this->getUsers(),
        ];
    }

	/**
	 * @return array<int,ControllerEntry>
	 */
	protected function getControllerEntries(): array
	{
		return [
			new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'announcement',
                AnnouncementController::class,
                'getDashboardWidget'),
		];
	}

	/**
	 * @return array<int,MenupointEntry>
	 */
	protected function getMenuPoints(): array
	{
        $routeName = AnnouncementController::ROUTE_NAME . 'index';
		$main = 'announcement.main';

		$menuPoints = [
			new MenupointEntry('announcement.manager',  $routeName, ['id' => 'manager'], $this->routeType, 3, [self::MANAGER_GROUP], $main),
			new MenupointEntry('announcement.author', $routeName, ['id' => 'author'], $this->routeType, 2, [self::AUTHOR_GROUP], $main),
			new MenupointEntry('announcement.member', $routeName, [], $this->routeType, 1, [self::USER_GROUP], $main),
			new MenupointEntry($main, '', [], $this->menuType, 6, [], BlogData::MENUPOINT_BLOG_MANAGE),
		];
		return [self::PACKAGE => $this->setSysAndActive($menuPoints)];
	}

    protected function getAuthData(): array
    {
        return [
            self::GROUP_TYPE_ANNOUNCEMENT => [
                self::MANAGER_GROUP => [
                    self::MANAGER_ROLE => 'announcement.manager',
                    self::AUTHOR_ROLE => 'announcement.author',
                    self::USER_ROLE => 'announcement.user',
                ],
                self::AUTHOR_GROUP => [
                    self::AUTHOR_ROLE => 'announcement.author',
                    self::USER_ROLE => 'announcement.user',
                ],
                self::USER_GROUP => [
                    self::USER_ROLE => 'announcement.user',
                ],
            ],
        ];
    }

    protected function getNotificationTypesData(): array
    {
        return [
            self::NOTIFICATION_ANNOUNCEMENT_ENABLED,
        ];
    }
}