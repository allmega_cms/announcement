<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\EventSubscriber;

use Allmega\AnnouncementBundle\Events;
use Allmega\BlogBundle\Model\FlashesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlashSubscriber implements EventSubscriberInterface
{
    use FlashesTrait;

    public static function getSubscribedEvents(): array
    {
        return [
            Events::ANNOUNCEMENT_CREATED => 'addSuccessFlash',
            Events::ANNOUNCEMENT_UPDATED => 'addSuccessFlash',
            Events::ANNOUNCEMENT_DELETED => 'addSuccessFlash',
            Events::ANNOUNCEMENT_STATE_CHANGED => 'addSuccessFlash',
        ];
    }
}