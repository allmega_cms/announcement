<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\EventSubscriber;

use Allmega\AnnouncementBundle\{Data, Events};
use Allmega\AuthBundle\Repository\UserRepository;
use Allmega\BlogBundle\Utils\Params\NotificationParams;
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class NotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private UserRepository $userRepo,
        private MessageBusInterface $bus) {}

    public static function getSubscribedEvents(): array
    {
        return [
            Events::ANNOUNCEMENT_ENABLED => 'onAnnouncementEnabled',
        ];
    }

    /**
     * @throws ExceptionInterface
     */
    public function onAnnouncementEnabled(GenericEvent $event): void
    {
        $announcement = $event->getSubject();
        $author = $announcement->getCreator();

        $search  = ['%username%', '%title%', '%message%'];
        $replace = [
            $author->getFullname(),
            $announcement->getTitle(),
            $announcement->getMessage()
        ];
        
        $params = new NotificationParams(Data::NOTIFICATION_ANNOUNCEMENT_ENABLED, [], $search, $replace);
        
        $users = $this->userRepo->findByRoles([Data::USER_ROLE]);
        foreach ($users as $user) $params->addReceiver($user->getEmail());

        $this->bus->dispatch($params);
    }
}