<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\EventSubscriber;

use Allmega\BlogBundle\Entity\Tag;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Events as BlogEvents;
use Allmega\AuthBundle\Events as AuthEvents;
use Allmega\AnnouncementBundle\Entity\Announcement;
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Doctrine\ORM\EntityManagerInterface;

readonly class DeletionSubscriber implements EventSubscriberInterface
{
    public function __construct(private EntityManagerInterface $em) {}

    public static function getSubscribedEvents(): array
    {
        return [
            AuthEvents::USER_DELETE => 'handleUserRelations',
            BlogEvents::TAG_DELETED => 'handleTagRelations',
        ];
    }

    public function handleUserRelations(GenericEvent $event, string $eventName): void
    {
        $this->em
            ->getRepository(User::class)
            ->updateField(Announcement::class, 'creator', $event->getSubject())
            ->updateField(Announcement::class, 'editor', $event->getSubject());
    }

    public function handleTagRelations(GenericEvent $event, string $eventName): void
    {
        $this->em
            ->getRepository(Tag::class)
            ->deleteTagRelations('allmega_announcement__tag', $event);
    }
}