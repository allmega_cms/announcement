<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\Manager;

use Allmega\MediaBundle\Events as MediaEvents;
use Allmega\AnnouncementBundle\{Data, Events};
use Allmega\AnnouncementBundle\Entity\Announcement;
use Allmega\AnnouncementBundle\Form\AnnouncementType;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\AnnouncementBundle\Controller\AnnouncementController;
use Symfony\Component\HttpFoundation\Response;
use DateTime;

trait AnnouncementControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
                $this->handleAnnouncementState();
                $this->params
                    ->getEntity()
                    ->setUpdated()
                    ->setEditor($this->getUser());
                break;
            case $this->delete:
                $this->params->getEntity()->getTags()->clear();
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $this->handleAnnouncementFilesAndState();
                break;
            case $this->delete:
                $this->deleteAnnouncementFiles();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $this->params
                    ->setRouteName($this->params->getRouteShort(), $this->show)
                    ->addRouteParams((['id' => $this->params->getEntity()->getId()]));
                break;
            case $this->delete:
                $this->params->addRouteParams((['id' => 'author']));
                break;
            default:
        }
        return $this;
    }

    private function handleAnnouncementState(): void
    {
        $active = $this->params->getEntity()->isActive();
        $isActive = $this->params->getAction() == $this->state ? !$active : $active;

        $publish = null;
        if ($isActive) {
            $publish = $this->params->getEntity()->getPublish() ?? new DateTime();
        }
        $this->params->getEntity()->setPublish($publish);
    }

    private function handleAnnouncementFilesAndState(): static
    {
        if ($this->params->getAction() != $this->state) {
            $this->saveFiles($this->params->getEntity()->getMediaDir(), $this->getFormFiles());
        }

        if ($this->params->getEntity()->isActive()) {
            $this->params->setEventName(Events::ANNOUNCEMENT_ENABLED);
            $this->dispatchEvent();
        }
        return $this;
    }

    private function deleteAnnouncementFiles(): void
    {
        $this->params
            ->addArguments(['folder' => $this->params->getEntity()->getMediaDir()])
            ->setEventName(MediaEvents::DELETE_FILES);
        
        $this->dispatchEvent();
    }

    private function save(Announcement $announcement = null, array $arguments = []): Response
    {
        $user = $this->getUser();
        $eventName = $announcement ? Events::ANNOUNCEMENT_UPDATED : Events::ANNOUNCEMENT_CREATED;
        $announcement = $announcement ?? (new Announcement())->setCreator($user)->setEditor($user);

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $announcement],
            entity: $announcement,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: AnnouncementType::class,
            routeName: AnnouncementController::ROUTE_NAME,
            templatesPath: AnnouncementController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}