<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\Security;

use Allmega\AuthBundle\Entity\User;
use Allmega\AnnouncementBundle\Data;
use Allmega\AnnouncementBundle\Entity\Announcement;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AnnouncementVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'announcement');
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        $isUser = $this->hasRole($user, Data::USER_ROLE);
        if (!$isUser || !$this->isSettedAndSupports($attribute, $subject)) return false;

        $isManager = $this->hasRole($user, Data::MANAGER_ROLE);
        $isAuthor = $this->hasRole($user, Data::AUTHOR_ROLE);

        switch ($attribute) {
            case $this->dashboard:
            case $this->search:
            case $this->list:
            case $this->show:
                $result = true;
                break;
            case $this->files:
            case $this->add:
                $result = $isManager || $isAuthor;
                break;
            case $this->delete:
            case $this->state:
            case $this->edit:
                $isSameUser = $this->isSameUser($user, $subject->getCreator());
                $result = $isManager || $isAuthor && $isSameUser;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Announcement;
    }
}
