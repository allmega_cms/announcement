<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\Controller;

use Allmega\AnnouncementBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\AnnouncementBundle\Entity\Announcement;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\AnnouncementBundle\Security\AnnouncementVoter;
use Allmega\AnnouncementBundle\Repository\AnnouncementRepository;
use Allmega\AnnouncementBundle\Manager\AnnouncementControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Utils\Params\{BaseControllerParams, RepoParams, TemplateParams, UTypes};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(name: 'allmega_announcement_')]
class AnnouncementController extends BaseController
{
    use AnnouncementControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaAnnouncement/announcement/';
    public const ROUTE_NAME = 'allmega_announcement_';
    public const PROP = 'announcement';

    public function __construct(
        private readonly AnnouncementRepository $announcementRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search/{type}', name: 'search', requirements: ['type' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('announcement-search')]
    public function search(SearchItem $item, string $type = UTypes::MEMBER): Response
    {
        $type = $this->checkType($type);
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setRouteParams(['type' => $type])
            ->setProps(['title'])
            ->getRepo()
            ->setSearch($this->announcementRepo)
            ->addOptions(['user' => $this->getUser(), 'type' => $type]);

        $item->getTrans()->setLabel('announcement.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list/{id}', name: 'index', requirements: ['id' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('announcement-list')]
    public function index(
        AnnouncementVoter $announcementVoter,
        Paginator $paginator,
        string $id = UTypes::MEMBER): Response
    {
        $type = $this->checkType($id);
        $repoParams = new RepoParams($this->getUser(), $type);
        $query = $this->announcementRepo->findAllQuery($repoParams);

        $optParams = [
            'data' => new TemplateParams(self::PROP, $type),
            'announcementVoter' => $announcementVoter,
        ];

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'item' => SortableItem::getInstance(new Announcement()),
            'announcements' => $paginator->getPagination($query),
        ]);
    }

    #[Route('/show/{type}/{id}', name: 'show', requirements: ['type' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('announcement-show', subject: self::PROP)]
    public function show(
        AnnouncementVoter $announcementVoter,
        Announcement $announcement,
        string $type = UTypes::MEMBER): Response
    {
        $type = $this->checkType($type);
        $optParams = [
            'data' => new TemplateParams(self::PROP, $type, true),
            'announcementVoter' => $announcementVoter,
            'announcement' => $announcement
        ];
        $params = [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams)
        ];

        if ($this->isXmlRequest) {
            $params['data']->setShow(false);
            $data = [
                'content' =>  $this->renderView(self::ROUTE_TEMPLATE_PATH . 'show/_layout.html.twig', $params),
            ];
            return $this->json($data);
        }
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', $params);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('announcement-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('announcement-edit', subject: self::PROP)]
    public function edit(Announcement $announcement): Response
    {
        return $this->save($announcement);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('announcement-delete', subject: self::PROP)]
    public function delete(Announcement $announcement): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $announcement,
            domain: Data::DOMAIN,
            eventName: Events::ANNOUNCEMENT_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('announcement-state', subject: self::PROP)]
    public function changeState(Announcement $announcement): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $announcement,
            domain: Data::DOMAIN,
            eventName: Events::ANNOUNCEMENT_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('announcement-dashboard')) return new Response();

        $user = $this->getUser();
        $latestNum = $this->announcementRepo->countLatest($user);

        $repoParams = new RepoParams($user, count: true);
        $num = $this->announcementRepo->findByRepoParams($repoParams);

        $repoParams->setCount(false)->setLimit(3);
        $announcements = $this->announcementRepo->findByRepoParams($repoParams);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'announcements' => $announcements, 'latestNum' => $latestNum, 'num' => $num,
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
        ]);
    }

    private function checkType(string $type): string
    {
        if ($type === UTypes::MANAGER && $this->isGranted(Data::MANAGER_ROLE)) {
            return $type;
        } elseif ($type === UTypes::AUTHOR && $this->isGranted(Data::AUTHOR_ROLE)) {
            return $type;
        } else {
            return UTypes::MEMBER;
        }
    }
}