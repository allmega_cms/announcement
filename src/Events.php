<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle;

final class Events
{
    public const ANNOUNCEMENT_CREATED = 'announcement.created';
    public const ANNOUNCEMENT_UPDATED = 'announcement.updated';
    public const ANNOUNCEMENT_DELETED = 'announcement.deleted';
    public const ANNOUNCEMENT_ENABLED = 'announcement.enabled';
    public const ANNOUNCEMENT_STATE_CHANGED = 'announcement.state_changed';
}