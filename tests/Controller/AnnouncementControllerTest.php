<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\AnnouncementBundle\Tests\Controller;

use Allmega\AnnouncementBundle\Controller\AnnouncementController;
use Allmega\AnnouncementBundle\Entity\Announcement;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\AnnouncementBundle\Data;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

class AnnouncementControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, Data::USER_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testSearch(): void
    {
        $this->runTests($this->search, Data::USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests($this->show, Data::USER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::AUTHOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testState(): void
    {
        $this->runModifyTests(
            role: Data::AUTHOR_ROLE,
            fnRedirectParams: 'getIdAsRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(
            role: Data::AUTHOR_ROLE,
            delete: true,
            redirectParams: ['id' => 'author']);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $user = $this->findUserByRole(Data::AUTHOR_ROLE);
        $entity = Announcement::build(user: $user);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return AnnouncementController::ROUTE_NAME . $name;
    }
}